<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Land;


class LandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lands = Land::all();

        return view("/layouts.pages.LandsList", ["lands" => $lands]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("/layouts.pages.create" );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return view("/layouts.pages.index" );
       //dd($request);
       $request->validate([
            "libelle"=>"required",
            "code_indicatif"=>"required|unique:lands,code_indicatif",
            "population"=>"required",
            "capital"=>"required",
            "superficie"=>"required",
            "laique"=>"required",
            "langue"=>"required",
            "monnaie"=>"required",
            "continent"=>"required",
            "description"=>"required"
        ]);

        //les clé sont les colonnes de la base donnée
        //les nom dans les get sont les name des input    
        Land::create([
            "Libelle"=>$request->get('libelle'),
            "code_indicatif"=>$request->get('code_indicatif'),
            "population"=>$request->get('population'),
            "capital"=>$request->get('capital'),
            "Superficie"=>$request->get('superficie'),
            "est_laique"=>$request->get('laique'),
            "langue"=>$request->get('langue'),
            "monnaie"=>$request->get('monnaie'),
            "Continent"=>$request->get('continent'),
            "description"=>$request->get('description')
        ]);
        return redirect()->route("lands.index");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $land = Land::findOrFail($id);
        // return view("/layouts.pages.create" );
        return view("/layouts.pages.edit", ['land' => $land]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Land $id)
    {
        $request->validate([
            "libelle"=>"required",
            "code_indicatif"=>"required",
            "population"=>"required",
            "capital"=>"required",
            "superficie"=>"required",
            "laique"=>"required",
            "langue"=>"required",
            "monnaie"=>"required",
            "continent"=>"required",
            "description"=>"required"
        ]);

        $id->update([
            "Libelle"=>$request->get('libelle'),
            "code_indicatif"=>$request->get('code_indicatif'),
            "population"=>$request->get('population'),
            "capital"=>$request->get('capital'),
            "Superficie"=>$request->get('superficie'),
            "est_laique"=>$request->get('laique'),
            "langue"=>$request->get('langue'),
            "monnaie"=>$request->get('monnaie'),
            "Continent"=>$request->get('continent'),
            "description"=>$request->get('description')
        ]);

        return redirect()->route("lands.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Land $id)
    {
        //Land::find($id)->delete();
        $id->delete();

        return redirect()->route("lands.index");//->with('success','ligue supprimée avec succès');

    }
}
