@extends('layouts.main');
@section('content')
    <div class="col-md-10">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Ajoutez un pays</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
              <form method="POST" action="{{ route('lands.store') }}">
                @csrf
                @method('POST')

                <div class="card-body">
                  <div class="form-group row">
                    <label for="code_indicatif" class="col-sm-2 col-form-label">Code indicatif</label>
                    <div class="col-sm-10">
                      <input required type="text" class="form-control" name="code_indicatif" id="code_indicatif" placeholder="+xxx"  value="{{ old('code_indicatif') }}">
                    </div>
                </div>

                <div class="form-group row">
                  <label for="libellé" class="col-sm-2 col-form-label">Libellé</label>
                  <div class="col-sm-10">
                    <input required type="text" class="form-control" name="libelle" id="libellé" placeholder="Côte d'Ivoire" value="{{ old('libelle') }}">
                  </div>
                </div>

                <div class="form-group row">
                      <label for="population" class="col-sm-2 col-form-label">Population</label>
                      <div class="col-sm-10">
                        <input required type="number" class="form-control" name="population" id="population" placeholder="Population" value="{{ old('population') }}">
                      </div>  
                </div>

                <div class="form-group row">
                      <label for="capital" class="col-sm-2 col-form-label">Capital</label>
                      <div class="col-sm-10">
                        <input required type="text" class="form-control" name="capital" id="capital" placeholder="Abidjan" value="{{ old('capital') }}">
                      </div>  
                  </div>

                <div class="form-group row">
                      <label for="superficie" class="col-sm-2 col-form-label">Superficie</label>
                      <div class="col-sm-10">
                        <input required type="number" class="form-control" name="superficie" id="superficie" placeholder="322462" value="{{ old('superficie') }}">
                      </div>  
                </div>

                <div class="form-group row">
                      <label for="laique" class="col-sm-2 col-form-label">Laïque</label>
                     <div>
                        <input required type="radio" name="laique" value="1" id="oui"/> <label for="oui">oui</label>
                        <input required type="radio" name="laique" value="0"id="non" /> <label for="non">non</label>
                    </div> 
                </div>

                <div class="form-group row">
                      <label for="langue" class="col-sm-2 col-form-label">Langue</label>
                      <div>
                        <input required type="radio" name="langue" value="FR" id="FR" value="{{ old('langue') }}"/> <label for="FR">FR</label>
                        <input required type="radio" name="langue" value="EN"id="EN"  value="{{ old('langue') }}"/> <label for="EN">EN</label>
                        <input required type="radio" name="langue" value="AR"id="AR"  value="{{ old('langue') }}"/> <label for="AR">AR</label>
                        <input required type="radio" name="langue" value="ES" id="ES"  value="{{ old('langue') }}"/> <label for="ES">ES</label>
                    </div>
                </div>

                <div class="form-group row">
                      <label for="Monnaie" class="col-sm-2 col-form-label">Monnaie</label>
                      <div>
                        <input required type="radio" name="monnaie" value="XOF" id="XOF" value="{{ old('monnaie') }}"/> <label for="XOF">XOF</label>
                        <input required type="radio" name="monnaie" value="EUR"id="EUR" value="{{ old('monnaie') }}" /> <label for="EUR">EUR</label>
                        <input required type="radio" name="monnaie" value="DOLLAR"id="DOLLAR" value="{{ old('monnaie') }}" /> <label for="DOLLAR">DOLLAR</label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="continent" class="col-sm-2 col-form-label">Contient</label>
                    <div>
                        <input required type="radio" name="continent" value="Afrique" id="Afrique" value="{{ old('continent') }}"/> <label for="Afrique">Afrique</label>
                        <input required type="radio" name="continent" value="Europe"id="Europe" value="{{ old('continent') }}"/> <label for="Europe">Europe</label>
                        <input required type="radio" name="continent" value="Amerique"id="Amerique" value="{{ old('continent') }}"/> <label for="Amerique">Amérique</label>
                        <input required type="radio" name="continent" value="Asie" id="Asie" value="{{ old('continent') }}"/> <label for="Asie">Asie</label>
                        <input required type="radio" name="continent" value="Océanie" id="Océanie" value="{{ old('continent') }}"/> <label for="Océanie">Océanie</label>
                        <input required type="radio" name="continent" value="Antartique" id="Antartique" value="{{ old('continent') }}"/> <label for="Antartique">Antartique</label>
                    </div>
                </div>

                <div class="form-group row">
                      <label for="description" class="col-sm-2 col-form-label">Description</label>
                      <div class="col-sm-10">
                        <textarea required name="description" id="description" cols="98" rows="10" placeholder="Décirvez le pays ici..." >{{ old('description') }}</textarea>
                      </div>  
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                    {{-- <button type="submit" class="btn btn-default float-right">Annuler</button> --}}
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->
            <style>
              #Afrique, #Europe, #Amerique, #Asie, #Océanie, #Antartique,
              #FR, #EN, #AR, #ES,
              #XOF, #EUR, #DOLLAR,
              #oui, #non
              {
                margin-left: 2em;
              }
            </style>

          </div>
@endsection