
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AdminLTE 3 | Dashboard</title>

  	@include('layouts.partials.css');
  
</head>
  <body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">

	  <!-- Preloader -->
	  <div class="preloader flex-column justify-content-center align-items-center">
		<img class="animation__shake" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
	  </div>

	  <!-- Navbar -->
	  @include('layouts.partials.navbar');
	  <!-- /.navbar -->

	  <!-- Main Sidebar Container -->
	  @include('layouts.partials.sidebar');

	  <!-- Content Wrapper. Contains page content -->
	  <div class="content-wrapper">
	  		@yield('content');
	  </div>
  
	  <!-- /.content-wrapper -->
	  	@include('layouts.partials.footer');

	  <!-- Control Sidebar -->
	  <aside class="control-sidebar control-sidebar-dark">
		<!-- Control sidebar content goes here -->
	  </aside>
	  <!-- /.control-sidebar -->
	</div>
	<!-- ./wrapper -->

	@include('layouts.partials.js');

	<Style>
	#content
	{
		background-color: orange;
		border: 2px solid red;
		/*width: 100vh;*/
		/*padding-left: 2em;*/
	}
</Style>

  </body>
</html>
